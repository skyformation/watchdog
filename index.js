'use strict';

const config = require('./config.json');
const q = require('q');
const fs = require('fs');
const spawn = require('child_process').spawn;
const path = require('path');
const os = require('os');
const _ = require('lodash');
const mkdirp = require('mkdirp');

const out = fs.openSync('./out.log', 'a');
const err = fs.openSync('./err.log', 'a');

validateRunning().fail(restartServer);

function restartServer(err) {

    const cmdShutdown = path.join(config.tomcatHome, 'bin/shutdown');
    const cmdStartup = path.join(config.tomcatHome, 'bin/startup');
    const binDir = path.join(config.tomcatHome, 'bin');
    const waitMs = 1000 * 20; // 20 sec.
    let cmd1, cmd2, cmd3;

    log('going to restart due to err: ', err);

    log('is windows: ', isWindows());
    if (isWindows()) {

        // cmd 1 - nice kill: shutdown.bat
        // cmd 2 - kill now: taskkill /F /IM java.exe
        // cmd 3 - startup: startup.sh
        cmd1 = ['shutdown.bat'];
        cmd2 = ['taskkill', '/F', '/IM', 'java.exe'];
        cmd3 = ['startup.bat'];

    } else {
        // assume *nix

        // cmd 1 - nice kill: shutdown.sh
        // cmd 2 - kill now: pkill -f java
        // cmd 3 - startup: startup.sh
        cmd1 = ['shutdown.sh'];
        cmd2 = ['pkill', '-f', 'java'];
        cmd3 = ['startup.sh'];

    }

    exec(binDir, cmd1);
    setTimeout(() => {
        exec(binDir, cmd2);
        setTimeout(() => {
            exec(binDir, cmd3);
            exit();
        }, waitMs);
    }, waitMs);

}

function exec(cwd, cmds) {
    log('going to exec cmd (', cwd, cmds[0], cmds.slice(1), '); ');
    let child = spawn(cmds[0], cmds.slice(1), {
        cwd: cwd,
        detached: true,
        stdio: ['ignore', out, err]
    });
    child.unref();
}

function isWindows() {
    return process.platform === 'win32';
}

function validateRunning() {
    var def = q.defer();
    var url = config.healthStatus;

    fs.readFile(url, {encoding: 'utf8'}, (err, data) => {

        if (err) {
            def.reject(err);
        } else {

            // simple: find latest event synced from all accounts
            // TODO: fid accounts that haven't synced (last sync is -1) and see if they still didnt sync next check

            if (data.length === 0) {
                // no accounts - valid
                def.resolve();
            } else {

                try {
                    let jsonBody = JSON.parse(data);

                    let managedAccounts = _.find(jsonBody, {id: 'managed-accounts'});
                    let accounts = _.orderBy(_.get(managedAccounts, 'accounts', []), ['lastSyncTime'], ['desc']);
                    if (accounts.length) {
                        let latestEventSyncTime = accounts[0].lastSyncTime;
                        let lastRecordedEventTime = getLastRecordedEventTime();
                        log('last saved event time:', lastRecordedEventTime);
                        log('latest event time:', latestEventSyncTime);
                        if (lastRecordedEventTime !== undefined && lastRecordedEventTime >= latestEventSyncTime) {
                            // no progress since last check
                            def.reject('no progress since last check');
                        } else {
                            // also handles case of first successful get of sync time ( undefined === lastRecordedEventTime )
                            def.resolve();
                            setLastRecordedEventTime(latestEventSyncTime);
                        }

                    } else {
                        // valid as no accounts to sync					
                        def.resolve();
                    }

                } catch (e) {
                    def.reject('Invalid structure of health object');
                }

            }

        }

    });


    return def.promise;
}

function exit() {
    process.exit(0);
}

function getConfigFile() {
    // our file is at <user home>/.skyformation/watchdog and should contain json of format:
    // { last-recorded-event-time: <ms since epoch> }
    // if file is not present - create it.
    // if record is not present on file - return undefined
    let result = {};
    let filePath = getConfigFilePath();

    let data = '';
    try {
        data = fs.readFileSync(filePath, {
            encoding: 'utf8',
            flag: 'r+'
        });
    } catch (e) {
        // file doesnt exist. create it
        log('getConfigFile: watchdog file doesnt exist. create it');
        deleteConfigFile();
        writeConfigFile();
    }


    if (data && data.length > 0) {
        try {
            result = JSON.parse(data);
        } catch (e) {
            // file is not in right format
            // delete it and re-create
            log('getConfigFile: watchdog file in invalid format. re-create it');
            deleteConfigFile();
            writeConfigFile();
        }
    }
    return result;

}

function getConfigFilePath() {
    // our file is at <user home>/.skyformation/watchdog
    // verify folder exists
    log('creating .skyformation folder at: ', path.join(homedir(), '.skyformation'));
    mkdirp(path.join(homedir(), '.skyformation'));
    return path.join(homedir(), '.skyformation', 'watchdog');
}

function getLastRecordedEventTime() {
    return getConfigFile()["last-recorded-event-time"];
}

function setLastRecordedEventTime(msTime) {
    let previousConfig = getConfigFile();
    previousConfig["last-recorded-event-time"] = msTime;
    writeConfigFile(previousConfig);
}

function writeConfigFile(config) {
    fs.writeFileSync(
        getConfigFilePath(),
        JSON.stringify(config || {}),
        {
            encoding: 'utf8',
            flag: 'w+'
        });
}

function deleteConfigFile() {
    try {
        fs.unlinkSync(
            getConfigFilePath()
        );
    } catch (e) {
        // if file doesnt exist its fine.
        // no way to handle other scenarios?
    }

}

function homedir() {
    return os.homedir();
}

function log() {
    var args = Array.prototype.slice.call(arguments);
    var file = path.join(homedir(), '.skyformation', 'skyformation-watchdog.txt');
    fs.writeFileSync(
        file,
        'AT: ' + new Date() + '\n\r',
        {
            encoding: 'utf8',
            flag: 'a+'
        });

    fs.writeFileSync(
        file,
        args.join(' ') + '\n\r',
        {
            encoding: 'utf8',
            flag: 'a+'
        });
}


